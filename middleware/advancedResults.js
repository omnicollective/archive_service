const advancedResults = (model, populate) => async (req, res, next) => {
    let query;
    // make select, sort, page, and limit acceptable params
    const reqQuery = { ...req.query };
    // Create array of fields to exclude
    const removeFields = ['select', 'sort', 'page', 'limit'];
    // Loop over removeFields and delete them from reqQuery
    removeFields.forEach(param => delete reqQuery[param]);


    // Create query string
    let queryStr = JSON.stringify(reqQuery);

    // Create operators $gt, $gte, etc.
    queryStr = queryStr.replace(/\b(gt|gte|lt|lte|in)\b/g, match => `$${match}`);

    // Finding resource
    query = model.find(JSON.parse(queryStr));

    // Select Fields
    if (req.query.select) {
      // using regex to select all occurances of "," and replace them with " "
      const fields = req.query.select.replace(/,/g, ' ');
      query = query.select(fields);
    }

    // Sort Fields
    // When sorting use a -paramToSortBy for ascending or no - for descending
    if (req.query.sort) {
      const sortBy = req.query.sort.replace(/,/g, ' ');
      query = query.sort(sortBy);
    } else {
      // default is sort by date,
      query = query.sort('-createdAt');
    }

    // Pagination
    // Radix is base 10. We either take in the req.query.page to be shown or the default page to be shown is 1
    const page = parseInt(req.query.page, 10) || 1;
    // Radix is base 10. We either take in the req.query.limit or the default limit, limit of items per page, is 100.
    const limit = parseInt(req.query.limit, 10) || 25;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const total = await model.countDocuments();

    query = query.skip(startIndex).limit(limit);

    if(populate) {
        query = query.populate(populate);
    }


    // Executing query
    const results = await query;

    // Pagination result
    const pagination = {};

    // Get next page if there are still more pages (for the front end)
    if (endIndex < total) {
      pagination.next = {
        page: page + 1,
        limit
      }
    }

    // Get previous page if there are any (for the front end)
    if (startIndex > 0) {
      pagination.prev = {
        page: page - 1,
        limit
      }
  
    }
    res.advancedResults = {
        success: true,
        count: results.length,
        pagination,
        data: results
    }
    next();

};

module.exports = advancedResults;